﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using mailNiftymebis_netcore.Entities;
using mailNiftymebis_netcore.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using X.PagedList;

namespace mailNiftymebis_netcore.Controllers
{
    [Authorize]
    public class MBSendController : Controller
    {
        private readonly MailDetailsModel mailDetailsModel;
        private readonly UserLoginModel userLoginModel;
        private readonly MailModel mailModel;

        private Mail mail = new Mail();
        private UserLogin userLogin = new UserLogin();
        private MailDetails mailDetails = new MailDetails();

        //private const string SessionId = "Id";
        //private const string SessionName = "Name";
        //private const string SessionSurname = "Surname";
        //private const string SessionEmail = "Email";

        public MBSendController(MailDetailsModel _mailDetailsModel, UserLoginModel _userLoginModel, MailModel _mailModel)
        {
            mailDetailsModel = _mailDetailsModel;
            userLoginModel = _userLoginModel;
            mailModel = _mailModel;
        }

        public IActionResult MBSend(string Sorting_Order, int? Page_No)/*int page = 1, int pageSize = 10*/
        {
            string userRefreshToken = HttpContext.User.Identity.Name.ToString();

            var user = userLoginModel.findAll().Where(x => x.RefreshToken == userRefreshToken).FirstOrDefault();

            string degerID = user.Id.ToString();
            var senderID = new ObjectId(degerID);
            var modelDetail = mailDetailsModel.findLookup().ToList();
            ViewBag.CurrentSortOrder = Sorting_Order;
            ViewBag.SortingDate = Sorting_Order == null || Sorting_Order == "near_date" ? "far_date" : "near_date";

            ViewBag.userName = user.Name;
            ViewBag.userSurname = user.Surname;
            ViewBag.userEmail = user.Email;

            switch (Sorting_Order)
            {
                case "far_date":
                    modelDetail = mailDetailsModel.findLookup().Where(x => x.Senderid == senderID && x.SenderSTATUS == false).ToList();
                    break;
                case "near_date":
                    modelDetail = mailDetailsModel.findLookup().Where(x => x.Senderid == senderID && x.SenderSTATUS == false).OrderByDescending(x => x.Mails[0].Date).ToList();
                    break;
                default:
                    modelDetail = mailDetailsModel.findLookup().Where(x => x.Senderid == senderID && x.SenderSTATUS == false).OrderByDescending(x => x.Mails[0].Date).ToList();
                    break;
            }

            var result = modelDetail.AsQueryable()
                .GroupBy(s => new { s.Mailid, s.Mails[0].Title, s.Mails[0].Message, s.Mails[0].Date })
                .Select(y => new Child { Mailid = y.Key.Mailid, Title = y.Key.Title, Message = y.Key.Message, Date = y.Key.Date, MailDetails = y.ToList() });
            var modelRead = mailDetailsModel.findLookup().Where(x => x.Takerid == senderID && x.StatusREAD == false).ToList();
            ViewBag.readCount = modelRead.Count();
            int Size_Of_Page = 5;
            int No_Of_Page = (Page_No ?? 1);

            PagedList<Child> detailsModel = new PagedList<Child>(result, No_Of_Page, Size_Of_Page);
            ViewBag.PagedListChild = detailsModel;
            return View(detailsModel);
        }

        [HttpGet]
        public IActionResult MBSendMES(string id)
        {

            string userRefreshToken = HttpContext.User.Identity.Name.ToString();

            var user = userLoginModel.findAll().Where(x => x.RefreshToken == userRefreshToken).FirstOrDefault();
            var result = userLoginModel.findAll();
            ViewBag.loginList = result;

            var currentAccount = mailModel.find(id);
            string deger = user.Id.ToString();

            var gondID = new ObjectId(deger);

            var ca = new ObjectId(id);

            var resultDATA = mailDetailsModel.findLookup().Where(x => x.Senderid == gondID && x.Mailid == ca).OrderByDescending(x => x.Mails[0].Date).ToList();

            ViewBag.result = resultDATA;

            var takerResult = resultDATA.AsQueryable().Where(x => x.Mailid == ca)
                .GroupBy(s => new { s.Mailid, s.Mails[0].Title, s.Mails[0].Message, s.Mails[0].Date })
                .Select(y => new Child { Mailid = y.Key.Mailid, Title = y.Key.Title, Message = y.Key.Message, Date = y.Key.Date, MailDetails = y.ToList() });

            ViewBag.takerInfo = takerResult;
            var modelRead = mailDetailsModel.findLookup().Where(x => x.Takerid == gondID && x.StatusREAD == false).ToList();

            ViewBag.readCount = modelRead.Count();

            return View("MBSendMES", currentAccount);
        }

        public void DataDelete(string id)
        {
            var docId = new ObjectId(id);

            List<MailDetails> details = mailDetailsModel.findAll().Where(x => x.Mailid == docId).ToList();

            var test = details;

            foreach (var item in test)
            {
                item.SenderSTATUS = true;
                var ID = item.Id;

                string rowID = ID.ToString();

                mailDetailsModel.update(rowID, item);
            }
        }

        [HttpPost]
        public JsonResult DeleteMessage(string id)
        {
            try
            {
                if (id != null)
                {
                    DataDelete(id);

                    return Json(new { result = true });
                }
                else
                {
                    return Json(new { result = false });
                }
            }
            catch (ArgumentNullException ne)
            {
                ne.ToString();
                return Json(new { Success = false });
            }
        }

        [HttpPost]
        public JsonResult DeleteMailSend(string[] deletedItems)
        {
            try
            {
                if (deletedItems != null)
                {
                    foreach (string id in deletedItems)
                    {
                        DataDelete(id);

                    }
                    return Json(new { result = true });
                }
                else
                {
                    return Json(new { result = false });
                }
            }
            catch (ArgumentNullException ne)
            {
                ne.ToString();
                return Json(new { Success = false });
            }
        }
        [HttpPost]
        public JsonResult MailSendCompose(string title, string message, string[] taker)
        {
            if (taker == null || message == null || title == null)
            {
                ViewBag.CreateSendError = "Hata oluştu lütfen kontrol ediniz ...";
                return Json(new { result = false });

            }
            else
            {

                string userRefreshToken = HttpContext.User.Identity.Name.ToString();

                var user = userLoginModel.findAll().Where(x => x.RefreshToken == userRefreshToken).FirstOrDefault();
                mail.Title = title;
                mail.Message = message;
                string tarih = DateTime.Now.ToString();
                string degerID = user.Id.ToString();
                var senderID = new ObjectId(degerID);
                mail.Date = Convert.ToDateTime(tarih);
                mail.Senderid = senderID;
                mail.SenderStatus = false;

                mailModel.create(mail);
                var mailID = mail.Id;
                mailDetails.Mailid = mailID;
                mailDetails.Senderid = senderID;
                var zeroId = new ObjectId();

                foreach (var item in taker)
                {
                    mailDetails.Takerid = new ObjectId(item);
                    mailDetailsModel.create(mailDetails);
                    mailDetails.Id = zeroId;
                }
                ViewBag.CreateSendSuccess = "Mesajiniz gonderildi ...";
                return Json(new { result = true });

            }

        }


    }
}
