﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using mailNiftymebis_netcore.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;

namespace mailNiftymebis_netcore.Controllers
{
    public class HomeController : Controller
    {
        private UserLoginModel userLoginModel;
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger, UserLoginModel _userLoginModel)
        {
            _logger = logger;
            userLoginModel = _userLoginModel;
        }

        [Authorize]
        public IActionResult Index()
        {
            //const string SessionId = "Id";
            //const string SessionName = "Name";
            //const string SessionSurname = "Surname";
            //const string SessionEmail = "Email";
            //ViewBag.userId = HttpContext.Session.GetString(SessionId);
            //ViewBag.userName = HttpContext.Session.GetString(SessionName);
            //ViewBag.userSurname = HttpContext.Session.GetString(SessionSurname);
            //ViewBag.userEmail = HttpContext.Session.GetString(SessionEmail);

            //ViewBag.cookieToken = HttpContext.User.Identity.Name.ToString();

            string userRefreshToken = HttpContext.User.Identity.Name.ToString();

            var user = userLoginModel.findAll().Where(x => x.RefreshToken == userRefreshToken).FirstOrDefault();

            ViewBag.userName    = user.Name;
            ViewBag.userSurname = user.Surname;
            ViewBag.userEmail   = user.Email;

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult Logout()
        {
            HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);

            //HttpContext.Response.Cookies.Delete("tokenUSER"); // tokeni silme
            
            return RedirectToAction("UserLogin", "UserLogin");
        }


    }
}
