﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using mailNiftymebis_netcore.Entities;
using mailNiftymebis_netcore.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace mailNiftymebis_netcore.Controllers
{
    [Authorize]
    public class MailComposeController : Controller
    {
        private readonly MailDetailsModel mailDetailsModel;
        private readonly UserLoginModel userLoginModel;
        private readonly MailModel mailModel;

        private Mail mail = new Mail();
        private UserLogin userLogin = new UserLogin();
        private MailDetails mailDetails = new MailDetails();

        public MailComposeController(MailDetailsModel _mailDetailsModel, UserLoginModel _userLoginModel, MailModel _mailModel)
        {
            mailDetailsModel = _mailDetailsModel;
            userLoginModel = _userLoginModel;
            mailModel = _mailModel;
        }
        public void DropListele()
        {
            var result = userLoginModel.findAll();
            ViewBag.loginList = result;
        }
        public IActionResult MailCompose()
        {
            //const string SessionId = "Id";
            //const string SessionName = "Name";
            //const string SessionSurname = "Surname";
            //const string SessionEmail = "Email";
            //ViewBag.userId = HttpContext.Session.GetString(SessionId);
            //ViewBag.userName = HttpContext.Session.GetString(SessionName);
            //ViewBag.userSurname = HttpContext.Session.GetString(SessionSurname);
            //ViewBag.userEmail = HttpContext.Session.GetString(SessionEmail);
            string userRefreshToken = HttpContext.User.Identity.Name.ToString();

            var user = userLoginModel.findAll().Where(x => x.RefreshToken == userRefreshToken).FirstOrDefault();

            ViewBag.userName = user.Name;
            ViewBag.userSurname = user.Surname;
            ViewBag.userEmail = user.Email;

            DropListele();

            string degerID = user.Id.ToString();
            var senderID = new ObjectId(degerID);

            var modelRead = mailDetailsModel.findLookup().Where(x => x.Takerid == senderID && x.StatusREAD == false).ToList();

            ViewBag.readCount = modelRead.Count();

            return View();
        }

        [HttpPost]
        public IActionResult MailCompose(string title, string message, string[] taker)
        {
            if (taker == null || message == null || title == null)
            {
                DropListele();
                return Json(new { result = false });
            }
            else
            {
                string userRefreshToken = HttpContext.User.Identity.Name.ToString();
                var user = userLoginModel.findAll().Where(x => x.RefreshToken == userRefreshToken).FirstOrDefault();

                mail.Title = title;
                mail.Message = message;
                string tarih = DateTime.Now.ToString();
                string degerID = user.Id.ToString();
                var senderID = new ObjectId(degerID);
                mail.Date = DateTime.Now;
                mail.Senderid = senderID;
                mail.SenderStatus = false;

                mailModel.create(mail);

                var mailID = mail.Id;
                mailDetails.Mailid = mailID;
                mailDetails.Senderid = senderID;

                var mailIDzero = new ObjectId();

                foreach (var item in taker)
                {
                    mailDetails.Takerid = new ObjectId(item);
                    mailDetailsModel.create(mailDetails);
                    mailDetails.Id = mailIDzero;

                }
                DropListele();
                ViewBag.CreateSendSuccess = "Mesajiniz gonderildi ...";
                return Json(new { result = true });

            }

        }



    }
}
