﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using mailNiftymebis_netcore.Entities;
using mailNiftymebis_netcore.Models;
using Microsoft.AspNetCore.Mvc;
using BC = BCrypt.Net.BCrypt;

namespace mailNiftymebis_netcore.Controllers
{
    public class UserRegisterController : Controller
    {
        private readonly UserLoginModel userLoginModel;


        public UserRegisterController(UserLoginModel _userLoginModel)
        {
            userLoginModel = _userLoginModel;
        }

        public IActionResult UserRegister()
        {
            return View();
        }

        [HttpPost]
        public JsonResult UserRegister(UserLogin userLogin)
        {
            if (userLogin == null)
            {
                return Json(new { Success =false});
            }
            else
            {
                var userObje = userLoginModel.findAll().Where(x => x.Email == userLogin.Email || x.Username == userLogin.Username).Count();
                if (userObje > 0)
                {
                    return Json(new { result = false });
                }
                else
                {
                    userLogin.Password = BC.HashPassword(userLogin.Password);
                    userLoginModel.create(userLogin);

                    return Json(new { result = true });
                }
            }
        }
    }
}
