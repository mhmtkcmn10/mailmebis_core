﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using mailNiftymebis_netcore.Entities;
using mailNiftymebis_netcore.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using X.PagedList;

namespace mailNiftymebis_netcore.Controllers
{
    [Authorize]
    public class MBInboxController : Controller
    {
        private readonly MailDetailsModel mailDetailsModel;
        private readonly UserLoginModel userLoginModel;
        private readonly MailModel mailModel;

        private Mail mail = new Mail();
        private UserLogin userLogin = new UserLogin();
        private MailDetails mailDetails = new MailDetails();


        //private const string SessionId = "Id";
        //private const string SessionName = "Name";
        //private const string SessionSurname = "Surname";
        //private const string SessionEmail = "Email";

        public MBInboxController(MailDetailsModel _mailDetailsModel, UserLoginModel _userLoginModel, MailModel _mailModel)
        {
            mailDetailsModel = _mailDetailsModel;
            userLoginModel = _userLoginModel;
            mailModel = _mailModel;
        }

        public IActionResult MBInbox(string Sorting_Order, int? Page_No)/*int page = 1, int pageSize = 10*/
        {
            string userRefreshToken = HttpContext.User.Identity.Name.ToString();

            var user = userLoginModel.findAll().Where(x => x.RefreshToken == userRefreshToken).FirstOrDefault();

            string degerID = user.Id.ToString();

            var takerID = new ObjectId(degerID);

            var modelDetail = mailDetailsModel.findLookup().ToList();

            ViewBag.CurrentSortOrder = Sorting_Order;
            ViewBag.SortingDate = Sorting_Order == null || Sorting_Order == "near_date" ? "far_date" : "near_date";

            ViewBag.userName = user.Name;
            ViewBag.userSurname = user.Surname;
            ViewBag.userEmail = user.Email;

            switch (Sorting_Order)
            {
                case "far_date":
                    modelDetail = mailDetailsModel.findLookup().Where(x => x.Takerid == takerID && x.TakerSTATUS == false).ToList();
                    break;
                case "near_date":
                    modelDetail = mailDetailsModel.findLookup().Where(x => x.Takerid == takerID && x.TakerSTATUS == false).OrderByDescending(x => x.Mails[0].Date).ToList();
                    break;
                default:
                    modelDetail = mailDetailsModel.findLookup().Where(x => x.Takerid == takerID && x.TakerSTATUS == false).OrderByDescending(x => x.Mails[0].Date).ToList();
                    break;
            }
            var modelRead = mailDetailsModel.findLookup().Where(x => x.Takerid == takerID && x.StatusREAD == false && x.TakerSTATUS == false).ToList();
            ViewBag.readCount = modelRead.Count();

            int Size_Of_Page = 5;
            int No_Of_Page = (Page_No ?? 1);
            PagedList<MailDetails> detailsModel = new PagedList<MailDetails>(modelDetail, No_Of_Page, Size_Of_Page);

            ViewBag.PagedListMailDetails = detailsModel;
            return View(detailsModel);
        }

        [HttpGet]
        public IActionResult MBInboxMes(string id)
        {
            string userRefreshToken = HttpContext.User.Identity.Name.ToString();
            var user = userLoginModel.findAll().Where(x => x.RefreshToken == userRefreshToken).FirstOrDefault();
            var userResult = userLoginModel.findAll();

            ViewBag.userName = user.Name;
            ViewBag.userSurname = user.Surname;
            ViewBag.userEmail = user.Email;

            ViewBag.loginList = userResult;

            var currentAccount = mailDetailsModel.find(id);

            string degerID = user.Id.ToString();
            var takerID = new ObjectId(degerID);
            var messageID = new ObjectId(id);

            var result = mailDetailsModel.findLookup().Where(x => x.Takerid == takerID && x.Id == messageID).OrderByDescending(x => x.Mails[0].Date).ToList();

            ViewBag.mailbilgi = result;
            var modelRead = mailDetailsModel.findLookup().Where(x => x.Takerid == takerID && x.StatusREAD == false).ToList();

            ViewBag.readCount = modelRead.Count();

            if (currentAccount.StatusREAD == false)
            {
                currentAccount.StatusREAD = true;
                mailDetailsModel.update(id, currentAccount);
            }
            return View("MBInboxMES", currentAccount);
        }

        public void DataDelete(string id)
        {
            var currentAccount = mailDetailsModel.find(id);
            currentAccount.TakerSTATUS = true;
            mailDetailsModel.update(id, currentAccount);
        }
        [HttpPost]
        public JsonResult MessageDelete(string id)
        {
            try
            {
                if (id != null)
                {
                    DataDelete(id);
                    return Json(new { result = true });
                }
                else
                {
                    return Json(new { result = false });
                }
            }
            catch (ArgumentNullException ne)
            {
                ne.ToString();
                return Json(new { Success = false });
            }
        }
        [HttpPost]
        public JsonResult DeleteMail(string[] deletedItems)
        {
            try
            {
                if (deletedItems != null)
                {
                    foreach (string inboxID in deletedItems)
                    {
                        DataDelete(inboxID);

                    }
                    return Json(new { result = true });
                }
                else
                {
                    return Json(new { result = false });
                }
            }
            catch (ArgumentNullException ne)
            {
                ne.ToString();
                return Json(new { Success = false });
            }
        }
        [HttpPost]
        public JsonResult MailUnRead(string[] updatedItems)
        {
            try
            {
                if (updatedItems != null)
                {
                    foreach (string inboxID in updatedItems)
                    {
                        var currentAccount = mailDetailsModel.find(inboxID);
                        currentAccount.StatusREAD = false;
                        mailDetailsModel.update(inboxID, currentAccount);
                    }
                    return Json(new { result = true });
                }
                else
                {
                    return Json(new { result = false });
                }
            }
            catch (ArgumentNullException ne)
            {
                ne.ToString();
                return Json(new { Success = false });
            }
        }
        [HttpPost]
        public JsonResult MailRead(string[] updatedItems)
        {
            try
            {
                if (updatedItems != null)
                {
                    foreach (string inboxID in updatedItems)
                    {
                        var currentAccount = mailDetailsModel.find(inboxID);
                        currentAccount.StatusREAD = true;
                        mailDetailsModel.update(inboxID, currentAccount);
                    }
                    return Json(new { result = true });
                }
                else
                {
                    return Json(new { result = false });
                }
            }
            catch (ArgumentNullException ne)
            {
                ne.ToString();
                return Json(new { result = false });
            }
        }
        [HttpPost]
        public JsonResult MailInboxCompose(string title, string message, string[] taker)
        {
            if (taker == null || message == null || title == null)
            {
                ViewBag.CreateSendError = "Hata oluştu lütfen kontrol ediniz ...";
                return Json(new { result = false });
            }

            else
            {
                string userRefreshToken = HttpContext.User.Identity.Name.ToString();
                var user = userLoginModel.findAll().Where(x => x.RefreshToken == userRefreshToken).FirstOrDefault();

                mail.Title = title;
                mail.Message = message;
                string tarih = DateTime.Now.ToString();
                string degerID = user.Id.ToString();
                var senderID = new ObjectId(degerID);
                mail.Date = Convert.ToDateTime(tarih);
                mail.Senderid = senderID;
                mail.SenderStatus = false;

                mailModel.create(mail);
                var mailID = mail.Id;
                mailDetails.Mailid = mailID;
                mailDetails.Senderid = senderID;
                var zeroID = new ObjectId();

                foreach (var item in taker)
                {
                    mailDetails.Takerid = new ObjectId(item);
                    mailDetailsModel.create(mailDetails);
                    mailDetails.Id = zeroID;
                }
                ViewBag.CreateSendSuccess = "Mesajiniz gonderildi ...";
                return Json(new { result = true });

            }
        }

    }
}
