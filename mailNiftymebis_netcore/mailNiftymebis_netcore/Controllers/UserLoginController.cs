﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using mailNiftymebis_netcore.Entities;
using mailNiftymebis_netcore.Models;
using mailNiftymebis_netcore.Token;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using BC = BCrypt.Net.BCrypt;


namespace mailNiftymebis_netcore.Controllers
{
    public class UserLoginController : Controller
    {
        private readonly UserLoginModel userLoginModel;

        readonly IConfiguration configuration;

        public UserLoginController(UserLoginModel _userLoginModel, IConfiguration _configuration)
        {
            configuration = _configuration;
            userLoginModel = _userLoginModel;
        }
        public IActionResult UserLogin()
        {
            return View();
        }

        [HttpPost]
        public JsonResult UserLogin(UserLoginInfo userLoginInfo)
        {
            UserLogin userLogin = userLoginModel.findAll()
                                 .Where(x => x.Username == userLoginInfo.Username ).FirstOrDefault(); //&& x.Password == userLoginInfo.Password


            if (userLogin == null || !BC.Verify(userLoginInfo.Password, userLogin.Password))
            {
                ViewBag.LoginError = "Hatalı Giriş yaptınız ... Kontrol ediniz ...";
                return Json(new { result = false });
            }
            else
            {
                //Token üretiliyor.
                TokenHandler tokenHandler = new TokenHandler(configuration);
                Token.Token token = tokenHandler.CreateAccessToken(userLogin);

                //Refresh token Users tablosuna işleniyor.
                userLogin.RefreshToken = token.RefreshToken;
                userLogin.RefreshTokenEndDate = token.Expiration.AddSeconds(30);
                string id = userLogin.Id.ToString();
                userLoginModel.update(id, userLogin);

                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name    ,userLogin.RefreshToken)
                    //,new Claim("AccessTokenDeger",token.AccessToken)
                };

                //CookieOptions cookie = new CookieOptions();
                //cookie.Expires = DateTime.Now.AddMinutes(5);
                //Response.Cookies.Append("tokenUSER", userLogin.RefreshTokenEndDate.ToString(), cookie);

                var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var principal = new ClaimsPrincipal(identity);
                var props = new AuthenticationProperties();
                HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal, props);


                return Json(new { result = true });
            }
        }
        [HttpPost]
        public JsonResult RefreshToken(/*string refreshToken*/)
        {
            UserLogin userLogin = userLoginModel.findAll().Where(x => x.RefreshToken == HttpContext.User.Identity.Name).FirstOrDefault();

            if (userLogin != null)/*&& user?.RefreshTokenEndDate > DateTime.Now*/
            {
                TokenHandler tokenHandler = new TokenHandler(configuration);
                Token.Token token = tokenHandler.CreateAccessToken(userLogin);
                userLogin.RefreshToken = token.RefreshToken;
                userLogin.RefreshTokenEndDate = token.Expiration.AddSeconds(60);
                string id = userLogin.Id.ToString();
                userLoginModel.update(id, userLogin);

                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name,userLogin.RefreshToken)
                };

                var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                var principal = new ClaimsPrincipal(identity);
                var props = new AuthenticationProperties();
                HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal, props);
                return Json(new { result = true });
            }
            return Json(new { result = false });
        }
    }
}
