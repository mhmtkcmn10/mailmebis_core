﻿using mailNiftymebis_netcore.Data;
using mailNiftymebis_netcore.Entities;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mailNiftymebis_netcore.Models
{
    public class UserLoginModel : IRepository<UserLogin>
    {
        private readonly IMongoCollection<UserLogin> userLoginCollection;

        public UserLoginModel(IMongoDBContext settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            userLoginCollection = database.GetCollection<UserLogin>("User");
        }


        public void create(UserLogin entity)
        {
            userLoginCollection.InsertOne(entity);
        }

        public void delete(string id)
        {
            var docId = new ObjectId(id);
            userLoginCollection.DeleteOne(m => m.Id == docId);
        }

        public UserLogin find(string id)
        {
            var docId = new ObjectId(id);
            return userLoginCollection.Find<UserLogin>(m => m.Id == docId).FirstOrDefault();
        }

        public List<UserLogin> findAll()
        {
            return userLoginCollection.AsQueryable<UserLogin>().ToList();
        }

        public void update(string id, UserLogin entity)
        {
            var docId = new ObjectId(id);
            userLoginCollection.ReplaceOne(m => m.Id == docId, entity);
        }
    }
}
