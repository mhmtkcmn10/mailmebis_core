﻿using mailNiftymebis_netcore.Data;
using mailNiftymebis_netcore.Entities;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mailNiftymebis_netcore.Models
{
    public class MailDetailsModel : IRepository<MailDetails>
    {
        public readonly IMongoCollection<MailDetails> mailDetailsCollection;
        public readonly IMongoCollection<UserLogin> userLoginCollection;
        public readonly IMongoCollection<Mail> mailCollection;

        public MailDetailsModel(IMongoDBContext settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            mailDetailsCollection = database.GetCollection<MailDetails>("MailDetails");
            userLoginCollection = database.GetCollection<UserLogin>("User");
            mailCollection = database.GetCollection<Mail>("Mail");

        }

        public void create(MailDetails entity)
        {
            mailDetailsCollection.InsertOne(entity);
        }

        public void delete(string id)
        {
            var docId = new ObjectId(id);
            mailDetailsCollection.DeleteOne(m => m.Id == docId);
        }

        public MailDetails find(string id)
        {
            var docId = new ObjectId(id);
            return mailDetailsCollection.Find<MailDetails>(m => m.Id == docId).FirstOrDefault();
        }
        public List<MailDetails> findAll()
        {
            return mailDetailsCollection.AsQueryable<MailDetails>().ToList();
        }

        public void update(string id, MailDetails entity)
        {
            var docId = new ObjectId(id);
            mailDetailsCollection.ReplaceOne(m => m.Id == docId, entity);
        }
        public List<MailDetails> findLookup()
        {
            List<MailDetails> result = mailDetailsCollection.Aggregate()
                .Lookup<MailDetails, UserLogin, MailDetails>(
                  foreignCollection: userLoginCollection,
                  localField: e => e.Senderid,
                  foreignField: f => f.Id,
                  @as: (MailDetails eo) => eo.UserLogins)
                .Lookup(
                  foreignCollection: userLoginCollection,
                  localField: e => e.Takerid,
                  foreignField: f => f.Id,
                  @as: (MailDetails eo) => eo.UserLogins2)
                  .Lookup(
                  foreignCollection: mailCollection,
                  localField: e => e.Mailid,
                  foreignField: f => f.Id,
                  @as: (MailDetails eo) => eo.Mails)
                .ToList();

            return result;
        }
    }
}
