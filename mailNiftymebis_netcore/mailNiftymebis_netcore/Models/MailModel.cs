﻿using mailNiftymebis_netcore.Data;
using mailNiftymebis_netcore.Entities;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mailNiftymebis_netcore.Models
{
    public class MailModel:IRepository<Mail>
    {
        private readonly IMongoCollection<MailDetails> mailDetailsCollection;
        private readonly IMongoCollection<UserLogin> userLoginCollection;
        private readonly IMongoCollection<Mail> mailCollection;


        public MailModel(IMongoDBContext settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);
            mailCollection = database.GetCollection<Mail>("Mail");
        }

        public void create(Mail entity)
        {
            mailCollection.InsertOne(entity);
        }

        public void delete(string id)
        {
            var docId = new ObjectId(id);
            mailCollection.DeleteOne(m => m.Id == docId);
        }

        public Mail find(string id)
        {
            var docId = new ObjectId(id);
            return mailCollection.Find<Mail>(m => m.Id == docId).FirstOrDefault();
        }
        public List<Mail> findAll()
        {
            return mailCollection.AsQueryable<Mail>().ToList();
        }

        public void update(string id, Mail entity)
        {
            var docId = new ObjectId(id);
            mailCollection.ReplaceOne(m => m.Id == docId, entity);
        }
    }
}
