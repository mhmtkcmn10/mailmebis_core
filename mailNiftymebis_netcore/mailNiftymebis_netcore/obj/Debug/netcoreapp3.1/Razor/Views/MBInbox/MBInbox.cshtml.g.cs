#pragma checksum "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\MBInbox\MBInbox.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "51e1ae91e45d01f8b1141ed109636ea91eeea3e3"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_MBInbox_MBInbox), @"mvc.1.0.view", @"/Views/MBInbox/MBInbox.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\_ViewImports.cshtml"
using mailNiftymebis_netcore;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\_ViewImports.cshtml"
using mailNiftymebis_netcore.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\MBInbox\MBInbox.cshtml"
using X.PagedList.Mvc.Core;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\MBInbox\MBInbox.cshtml"
using X.PagedList;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\MBInbox\MBInbox.cshtml"
using X.PagedList.Mvc.Core.Common;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"51e1ae91e45d01f8b1141ed109636ea91eeea3e3", @"/Views/MBInbox/MBInbox.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"d61746cd77d38f309c2f6c7fde1baebf2d8c502d", @"/Views/_ViewImports.cshtml")]
    public class Views_MBInbox_MBInbox : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("href", new global::Microsoft.AspNetCore.Html.HtmlString("~/MailCompose/MailCompose"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("btn btn-block btn-success"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("href", new global::Microsoft.AspNetCore.Html.HtmlString("~/MBInbox/MBInbox"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("list-group-item mail-nav-unread"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("href", new global::Microsoft.AspNetCore.Html.HtmlString("~/MBSend/MBSend"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_5 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("list-group-item "), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\n");
#nullable restore
#line 6 "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\MBInbox\MBInbox.cshtml"
   ViewBag.Title = "MBInbox";
    Layout = "~/Views/Shared/_LayoutMasterPage.cshtml"; 

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<div id=""content-container"">
    <div id=""page-head"">
    </div>

    <div id=""page-content"">

        <div class=""panel"">
            <div class=""panel-body"">
                <div class=""fixed-fluid"">
                    <div class=""fixed-sm-200 pull-sm-left fixed-right-border"">

                        <div class=""pad-btm bord-btm"">
                            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "51e1ae91e45d01f8b1141ed109636ea91eeea3e36847", async() => {
                WriteLiteral("Compose Mail");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\n                        </div>\n\n                        <p class=\"pad-hor mar-top text-main text-bold text-sm text-uppercase\">Folders</p>\n                        <div class=\"list-group bg-trans pad-btm bord-btm\">\n                            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "51e1ae91e45d01f8b1141ed109636ea91eeea3e38258", async() => {
                WriteLiteral("\n                                <i class=\"demo-pli-mail-unread icon-lg icon-fw\"></i> Inbox (");
#nullable restore
#line 27 "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\MBInbox\MBInbox.cshtml"
                                                                                       Write(ViewBag.readCount);

#line default
#line hidden
#nullable disable
                WriteLiteral(")\n                            ");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\n                            ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "51e1ae91e45d01f8b1141ed109636ea91eeea3e39920", async() => {
                WriteLiteral("\n                                <i class=\"demo-pli-mail-send icon-lg icon-fw\"></i> Send\n                            ");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_5);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
                        </div>
                    </div>

                    <div class=""panel-body"" id=""loader"" style=""display:none; position:absolute; left:45%; top:30%;"">
                        <div class=""load4"">
                            <div class=""loader""></div>
                        </div>
                    </div>
                    <div class=""fluid"" id=""container-animation"">
                        <div id=""demo-email-list"">
                            <div id=""kutucuklar"">
                                <div class=""row"">
                                    <div class=""col-sm-7 toolbar-left"">

                                        <div class=""btn-group"">
                                            <label id=""demo-checked-all-mail"" for=""select-all-mail"" class=""btn btn-default"">
                                                <input type=""checkbox"" id=""select_all"" />
                                            </label>
                                            <button data-toggle=""drop");
            WriteLiteral(@"down"" class=""btn btn-default dropdown-toggle""><i class=""dropdown-caret""></i></button>
                                            <ul class=""dropdown-menu"">
                                                <li><a href=""#"" id=""btnAll"">Tümü</a></li>
                                                <li><a href=""#"" id=""btnNone"">Hiçbiri</a></li>
                                                <li class=""divider""></li>
                                                <li><a href=""#"" id=""btnAllRead"">Okundu</a></li>
                                                <li><a href=""#"" id=""btnAllUnRead"">Okunmadı</a></li>
                                            </ul>
                                        </div>

                                        <!--Refresh button-->

                                        <button id=""demo-mail-ref-btn"" data - toggle=""panel-overlay"" data-target=""#demo-email-list"" class=""btn btn-default"" type=""button""");
            BeginWriteAttribute("onclick", " onclick=\"", 3326, "\"", 3428, 3);
            WriteAttributeValue("", 3336, "location.href=\'", 3336, 15, true);
#nullable restore
#line 62 "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\MBInbox\MBInbox.cshtml"
WriteAttributeValue("", 3351, Url.Action("MBInbox", "MBInbox",new { Sorting_Order = ViewBag.SortingDate}), 3351, 76, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 3427, "\'", 3427, 1, true);
            EndWriteAttribute();
            WriteLiteral(@">
                                            <i class=""demo-psi-repeat-2""></i>
                                        </button>

                                        <!--Dropdown button (More Action)-->
                                        <div class=""btn-group dropdown"">
                                            <button data-toggle=""dropdown"" class=""btn btn-default dropdown-toggle"" type=""button"">
                                                Diğer <i class=""dropdown-caret""></i>
                                            </button>
                                            <ul class=""dropdown-menu"">
                                                <li><a href=""#"" id=""btnRead"">Okundu İşaretle</a></li>
                                                <li><a href=""#"" id=""btnUnRead"">Okunmadı İşaretle</a></li>
                                                <li><a href=""#"" id=""btnAllDelete"">Tümünü Sil</a></li>
                                            </ul>
                                        </d");
            WriteLiteral(@"iv>
                                    </div>
                                    <div class=""col-sm-5 toolbar-right"">
                                        <!--Pager buttons-->
                                        <span class=""text-main"">
                                            <strong>");
#nullable restore
#line 81 "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\MBInbox\MBInbox.cshtml"
                                               Write(Model.FirstItemOnPage);

#line default
#line hidden
#nullable disable
            WriteLiteral(" - ");
#nullable restore
#line 81 "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\MBInbox\MBInbox.cshtml"
                                                                        Write(Model.LastItemOnPage);

#line default
#line hidden
#nullable disable
            WriteLiteral("</strong>\n                                            of\n                                            <strong>");
#nullable restore
#line 83 "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\MBInbox\MBInbox.cshtml"
                                               Write(Model.TotalItemCount);

#line default
#line hidden
#nullable disable
            WriteLiteral("</strong>\n");
            WriteLiteral("                                        </span>\n                                        <div class=\"btn-group btn-group\">\n");
            WriteLiteral("                                            ");
#nullable restore
#line 94 "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\MBInbox\MBInbox.cshtml"
                                       Write(Html.PagedListPager((IPagedList)ViewBag.PagedListMailDetails, Page_No => Url.Action("MBInbox",
                                                               new { Page_No, Sorting_Order = ViewBag.CurrentSortOrder }), PagedListRenderOptions.Minimal));

#line default
#line hidden
#nullable disable
            WriteLiteral(@"

                                        </div>
                                    </div>
                                </div>
                                <!--Mail list group-->

                                <ul id=""demo-mail-list"" class=""mail-list pad-top bord-top"">
");
#nullable restore
#line 103 "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\MBInbox\MBInbox.cshtml"
                                     foreach (var mail in Model)
                                    {

#line default
#line hidden
#nullable disable
            WriteLiteral("                        <li");
            BeginWriteAttribute("class", " class=\"", 6559, "\"", 6633, 2);
            WriteAttributeValue("", 6567, "mail-starred", 6567, 12, true);
#nullable restore
#line 105 "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\MBInbox\MBInbox.cshtml"
WriteAttributeValue(" ", 6579, mail.StatusREAD == false ? "mail-list-unread" : "", 6580, 53, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\n                            <div class=\"mail-control\">\n                                <input type=\"checkbox\" name=\"checkBox\"");
            BeginWriteAttribute("class", " class=\"", 6761, "\"", 6820, 2);
            WriteAttributeValue("", 6769, "checkBox", 6769, 8, true);
#nullable restore
#line 107 "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\MBInbox\MBInbox.cshtml"
WriteAttributeValue("", 6777, mail.StatusREAD == false ? "unread" : "", 6777, 43, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            BeginWriteAttribute("value", " value=\"", 6821, "\"", 6837, 1);
#nullable restore
#line 107 "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\MBInbox\MBInbox.cshtml"
WriteAttributeValue("", 6829, mail.Id, 6829, 8, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\n                            </div>\n                            <div class=\"mail-star\"><a href=\"#\"><i class=\"demo-psi-star\"></i></a></div>\n                            <div class=\"mail-from\">\n\n                                <a");
            BeginWriteAttribute("href", " href=\"", 7065, "\"", 7130, 1);
#nullable restore
#line 112 "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\MBInbox\MBInbox.cshtml"
WriteAttributeValue("", 7072, Url.Action("MBInboxMES", "MBInbox", new { id = mail.Id }), 7072, 58, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\n                                    ");
#nullable restore
#line 113 "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\MBInbox\MBInbox.cshtml"
                               Write(mail.UserLogins[0].Name);

#line default
#line hidden
#nullable disable
            WriteLiteral(" ");
#nullable restore
#line 113 "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\MBInbox\MBInbox.cshtml"
                                                        Write(mail.UserLogins[0].Surname);

#line default
#line hidden
#nullable disable
            WriteLiteral("\n                                </a>\n                            </div>\n                            <div class=\"mail-time\">\n                                ");
#nullable restore
#line 117 "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\MBInbox\MBInbox.cshtml"
                           Write(mail.Mails[0].Date.ToString("dd.mm.yyyy"));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n\n                            </div>\n                            <div class=\"mail-attach-icon\"></div>\n                            <div class=\"mail-subject\">\n                                <a");
            BeginWriteAttribute("href", " href=\"", 7611, "\"", 7674, 1);
#nullable restore
#line 122 "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\MBInbox\MBInbox.cshtml"
WriteAttributeValue("", 7618, Url.Action("MBInboxMES", "MBInbox",  new { id=mail.Id}), 7618, 56, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\n                                    ");
#nullable restore
#line 123 "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\MBInbox\MBInbox.cshtml"
                               Write(mail.Mails[0].Title);

#line default
#line hidden
#nullable disable
            WriteLiteral("\n                                </a>\n                            </div>\n                        </li>");
#nullable restore
#line 126 "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\MBInbox\MBInbox.cshtml"
                             }

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                                </ul>
                                <div class=""modal fade"" id=""myModal"">
                                    <div class=""modal-dialog"">
                                        <div class=""modal-content"">
                                            <div class=""modal-header"">
                                                <button type=""button"" class=""close"" data-dismiss=""modal""><i class=""pci-cross pci-circle""></i></button>
                                                <h4 class=""modal-title"">Silme Onaylama</h4>
                                            </div>

                                            <div class=""modal-body"">
                                                <p>Silmek için emin misiniz ?</p>
                                            </div>

                                            <div class=""modal-footer"">
                                                <button class=""btn btn-default"" data-dismiss=""modal"" type=""button"">Close</button>
               ");
            WriteLiteral(@"                                 <button class=""btn btn-primary"" type=""button"" onclick=""DeleteMail()"">Confirm</button>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <!--Mail footer-->
                        <div class=""panel-footer clearfix"">
                            <div class=""pull-right"">
                                <span class=""text-main"">
                                    <strong>");
#nullable restore
#line 155 "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\MBInbox\MBInbox.cshtml"
                                       Write(Model.FirstItemOnPage);

#line default
#line hidden
#nullable disable
            WriteLiteral(" - ");
#nullable restore
#line 155 "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\MBInbox\MBInbox.cshtml"
                                                                Write(Model.LastItemOnPage);

#line default
#line hidden
#nullable disable
            WriteLiteral("</strong>\n                                    of\n                                    <strong>");
#nullable restore
#line 157 "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\MBInbox\MBInbox.cshtml"
                                       Write(Model.TotalItemCount);

#line default
#line hidden
#nullable disable
            WriteLiteral("</strong>\n                                </span>\n                                <div class=\"btn-group btn-group\">\n                                    ");
#nullable restore
#line 160 "C:\Users\mehmet.kocaman\source\repos\mailmebis_core\mailNiftymebis_netcore\mailNiftymebis_netcore\Views\MBInbox\MBInbox.cshtml"
                               Write(Html.PagedListPager((IPagedList)ViewBag.PagedListMailDetails, Page_No => Url.Action("MBInbox",
                                                               new { Page_No, Sorting_Order = ViewBag.CurrentSortOrder }), PagedListRenderOptions.Minimal));

#line default
#line hidden
#nullable disable
            WriteLiteral("\n\n                                </div>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n        </div>\n\n    </div>\n\n</div>\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
