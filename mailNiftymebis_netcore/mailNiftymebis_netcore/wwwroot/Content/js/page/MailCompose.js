﻿

$('#summernote').summernote({
    placeholder: '',
    tabsize: 2,
    height: 250,
    toolbar: [
        ['style', ['style']],
        ['font', ['bold', 'underline', 'clear']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['table', ['table']],
        ['insert', ['link', 'picture', 'video']],
        ['view', ['fullscreen', 'codeview', 'help']]
    ]
});


$(document).on('nifty.ready', function () {

    var btn_compose = $("#btnCompose")

    btn_compose.on("click", function (e) {
        e.preventDefault();
        var txttitle = $("#txtTitle").val()
        var txtsummer = $('#summernote').summernote('code')
        var SecilenKullanicilar = [];
        $('#demo-cs-multiselect :selected').each(function (i, selected) {
            SecilenKullanicilar[i] = $(selected).val();
        });
        if (txttitle == "" || txtsummer == "<p><br></p>" || txtsummer == "" || SecilenKullanicilar == "") {

            $.niftyNoty({
                type: 'danger',
                container: 'floating',
                html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-flash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hata</h4><p class="alert-message">Alanlar boş bırakılamaz</p></div>',
                closeBtn: true,
                floating: {
                    position: "top-right"
                },
                focus: true,
                timer: true ? 2000 : 0
            });
        }
        else {
            var valdata = $("#composeFORM").serialize();
            $.ajax({
                url: "/MailCompose/MailCompose",
                type: "POST",
                data: { title:$("#txtTitle").val(),message:$('#summernote').summernote('code'),taker: SecilenKullanicilar},
                dataType: "json",
                contentType:'application/x-www-form-urlencoded; charset=UTF-8',
                success: function (response) {
                    if (response.result==true) {
                        $.niftyNoty({
                            type: 'success',
                            container: 'floating',
                            html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-mail-send icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Başarılı</h4><p class="alert-message">Mesajınız gönderildi</p></div>',
                            closeBtn: true,
                            floating: {
                                position: "top-right"
                            },
                            focus: true,
                            timer: true ? 2000 : 0
                        });
                        setTimeout(function () { location.reload() }, 2000);
                    } else {
                        $.niftyNoty({
                            type: 'danger',
                            container: 'floating',
                            html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-flash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hatalı İşlem</h4><p class="alert-message">Bir hata ile karşılaşıldı</p></div>',
                            closeBtn: true,
                            floating: {
                                position: "top-right"
                            },
                            focus: true,
                            timer: true ? 2000 : 0
                        });
                        setTimeout(function () { location.reload() }, 2000);
                    }
                },
                error: function () {
                    $.niftyNoty({
                        type: 'danger',
                        container: 'floating',
                        html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-flash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hatalı İşlem</h4><p class="alert-message">Hata oluştu</p></div>',
                        closeBtn: true,
                        floating: {
                            position: "top-right"
                        },
                        focus: true,
                        timer: true ? 2000 : 0
                    });
                    setTimeout(function () { location.reload() }, 2000);
                }
            });
        }
    })
})