﻿
$(document).on('nifty.ready', function () {

    $('#btnYanitla').on('click', function () {

        $('#summernote').summernote({
            placeholder: '',
            tabsize: 2,
            height: 250,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
        $('#demo-mail-textarea').hide();
        $('#btnCompose').removeClass('hide');
        $('#konutext').removeClass('hide');
    });

    $('#btnYonlendir').on('click', function () {
        $('#summernote').summernote({
            placeholder: '',
            tabsize: 2,
            height: 250,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture', 'video']],
                ['view', ['fullscreen', 'codeview', 'help']]
            ]
        });
        $('#demo-mail-textarea').hide();
        $('#btnCompose').removeClass('hide');
        $('#konutext').removeClass('hide');

    });
    if ($('#demo-mail-textarea').length) {
        $('#demo-mail-textarea').on('click', function () {
            $('#summernote').summernote({
                placeholder: '',
                tabsize: 2,
                height: 250,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                    ['insert', ['link', 'picture', 'video']],
                    ['view', ['fullscreen', 'codeview', 'help']]
                ]
            });
            $('#demo-mail-textarea').hide();
            $('#btnCompose').removeClass('hide');
            $('#konutext').removeClass('hide');
        });
        return;
    }
});

$("#btnMessageRemove").click(function (e) {
    $("#myModal").modal('show');
});
var DeleteMailMessage = function () {

    var ID = $('#btnDeleteMailMes').attr('name');
    $.ajax({
        url: '/MBInbox/MessageDelete/' + ID,
        type: 'POST',
        data: ID,
        success: function (data) {
            if (data.result==true) {
                $("#myModal").modal('hide');
                $.niftyNoty({
                    type: 'success',
                    container: 'floating',
                    html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-trash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Başarılı</h4><p class="alert-message">Mesaj Silindi</p></div>',
                    closeBtn: true,
                    floating: {
                        position: "top-right"
                    },
                    focus: true,
                    timer: true ? 2500 : 0
                });
                setTimeout(function () { window.location.href = "/MBInbox/MBInbox" }, 2000);
            } else {
                $.niftyNoty({
                    type: 'danger',
                    container: 'floating',
                    html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-flash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hata</h4><p class="alert-message">Hatalı İşlem</p></div>',
                    closeBtn: true,
                    floating: {
                        position: "top-right"
                    },
                    focus: true,
                    timer: true ? 2500 : 0
                });

                setTimeout(function () { location.reload() }, 2000);
            }
        },
        error: function () {

            $.niftyNoty({
                type: 'danger',
                container: 'floating',
                html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-flash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hata</h4><p class="alert-message">Hatalı İşlem</p></div>',
                closeBtn: true,
                floating: {
                    position: "top-right"
                },
                focus: true,
                timer: true ? 2500 : 0
            });
            setTimeout(function () { location.reload() }, 2000);
        }
    });

}
var btn_compose = $("#btnCompose")

btn_compose.on("click", function (e) {
    e.preventDefault();
    var txtsubject = $("#inputSubject").val()
    var txtsummer = $('#summernote').summernote('code')
    var SecilenKullanicilar = [];

    $('#demo-cs-multiselect :selected').each(function (i, selected) {
        SecilenKullanicilar[i] = $(selected).val();
    });
    if (txtsubject == "" || txtsummer == "<p><br></p>" || txtsummer == "" || SecilenKullanicilar == "") {

        $.niftyNoty({
            type: 'danger',
            container: 'floating',
            html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-flash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hata</h4><p class="alert-message">Alanlar boş bırakılamaz</p></div>',
            closeBtn: true,
            floating: {
                position: "top-right"
            },
            focus: true,
            timer: true ? 2000 : 0
        });
    }
    else {
        var data = {
            "title": txtsubject,
            "message":txtsummer,
            "taker": SecilenKullanicilar
        }
        $.ajax({
            url: "/MBInbox/MailInboxCompose",
            type: "POST",
            data: { title:txtsubject, message:txtsummer , taker:SecilenKullanicilar },
            dataType: "json",
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            success: function (response) {
                if (response.result==true) {
                    $.niftyNoty({
                        type: 'success',
                        container: 'floating',
                        html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-mail-send icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Başarılı</h4><p class="alert-message">Mesajınız gönderildi</p></div>',
                        closeBtn: true,
                        floating: {
                            position: "top-right"
                        },
                        focus: true,
                        timer: true ? 2000 : 0
                    });
                    setTimeout(function () { location.reload() }, 2000);
                }
                else {
                    $.niftyNoty({
                        type: 'danger',
                        container: 'floating',
                        html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-flash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hatalı İşlem</h4><p class="alert-message">Bir hata ile karşılaşıldı</p></div>',
                        closeBtn: true,
                        floating: {
                            position: "top-right"
                        },
                        focus: true,
                        timer: true ? 2000 : 0
                    });
                    setTimeout(function () { location.reload() }, 2000);
                }
            },
            error: function () {
                $.niftyNoty({
                    type: 'danger',
                    container: 'floating',
                    html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-flash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hatalı İşlem</h4><p class="alert-message">Hata oluştu</p></div>',
                    closeBtn: true,
                    floating: {
                        position: "top-right"
                    },
                    focus: true,
                    timer: true ? 2000 : 0
                });
                setTimeout(function () { location.reload() }, 2000);
            }
        });
    }
    

})

