﻿
$(document).on('nifty.ready', function () {

    var btn_Login = $("#btnLogin")

    btn_Login.on("click", function (e) {
        e.preventDefault();

        var username = $("#usernamex").val();
        var password = $("#passwordx").val();

        if (username == "" || password == "") {
            $.niftyNoty({
                type: 'danger',
                container: 'floating',
                html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-flash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hatalı Giriş</h4><p class="alert-message">Alanlar boş bırakılamaz</p></div>',
                closeBtn: true,
                floating: {
                    position: "top-right"
                },
                focus: true,
                timer: true ? 2000 : 0
            });
        } else {

            var data = {
                "username": username,
                "password": password
            }
            var valdata = $("#registerFORM").serialize();
            $.ajax({
                url: "/UserLogin/UserLogin",
                type: "POST",
                data: { Username: username, Password: password },
                dataType: "json",
                contentType: /*"application/json"*/'application/x-www-form-urlencoded; charset=UTF-8',
                beforeSend: function () {
                    $("#loader").show();
                    $("#anagovde").css({ "opacity": "0.5" });
                },
                success: function (response) {
                    if (response.result == true) {

                        setTimeout(function () {
                            $.get("/Home/Index", function (data) {
                                window.location.href = "/Home/Index"
                            });
                        }, 1500);

                    } else {

                        $("#loader").hide();
                        $("#anagovde").css({ "opacity": "1" });

                        $.niftyNoty({
                            type: 'danger',
                            container: 'floating',
                            html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-flash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hatalı Giriş</h4><p class="alert-message">Kullanıcı bilgilerinizi kontrol ediniz</p></div>',
                            closeBtn: true,
                            floating: {
                                position: "top-right"
                            },
                            focus: true,
                            timer: true ? 2000 : 0
                        });
                    }
                },
                error: function () {
                    $("#loader").hide();
                    $("#anagovde").css({ "opacity": "1" });

                    $.niftyNoty({
                        type: 'danger',
                        container: 'floating',
                        html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-flash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hatalı Giriş</h4><p class="alert-message">Hatalı giriş</p></div>',
                        closeBtn: true,
                        floating: {
                            position: "top-right"
                        },
                        focus: true,
                        timer: true ? 2000 : 0
                    });
                }
            })


        }

    })
})
