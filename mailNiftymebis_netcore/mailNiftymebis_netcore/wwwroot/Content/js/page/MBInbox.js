﻿

$(function () {
    $('#select_all').change(function () {
        var checkboxes = $(this).closest('#kutucuklar').find('.checkBox');
        var checkboxesunread = $(this).closest('#kutucuklar').find('.checkBoxunread');

        if ($(this).prop('checked')) {
            checkboxes.prop('checked', true);
            checkboxesunread.prop('checked', true);

        } else {
            checkboxes.prop('checked', false);
            checkboxesunread.prop('checked', false);

        }
    }
    );
});

$("#btnAllDelete").click(function (e) {
    var chckedlist = $("input:checkbox.checkBox:checked");
    var chckedlistunread = $("input:checkbox.checkBoxunread:checked");

    if (chckedlist.length > 0 || chckedlistunread.length > 0) {
        $("#myModal").modal('show');
    }
    else {
        $.niftyNoty({
            type: 'danger',
            container: 'floating',
            html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-flash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hata</h4><p class="alert-message">Hatalı İşlem</p></div>',
            closeBtn: true,
            floating: {
                position: "top-right"
            },
            focus: true,
            timer: true ? 2000 : 0
        });
    }
});
var DeleteMail = function () {
    var deletedItems = new Array();
    $('input:checkbox.checkBox').each(function () {
        if ($(this).prop('checked')) {
            deletedItems.push($(this).val());
        }
    });
    $('input:checkbox.checkBoxunread').each(function () {
        if ($(this).prop('checked')) {
            deletedItems.push($(this).val());
        }
    });
    var postData = JSON.stringify({
        deletedItems
    });

    $.ajax({
        url: "/MBInbox/DeleteMail",
        type: "POST",
        data: { deletedItems: deletedItems },
        dataType: "json",
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        success: function (response) {
            if (response.result==true) {
                $('tr:has(input[name="checkBox"]:checked)').remove();
                $("#myModal").modal("hide");
                setTimeout(function () { location.reload(); }, 2000);
                $.niftyNoty({
                    type: 'success',
                    container: 'floating',
                    html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-trash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Başarılı</h4><p class="alert-message">İşlem gerçekleşti</p></div>',
                    closeBtn: true,
                    floating: {
                        position: "top-right"
                    },
                    focus: true,
                    timer: true ? 2000 : 0
                });
            } else {
                $.niftyNoty({
                    type: 'danger',
                    container: 'floating',
                    html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-flash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hata</h4><p class="alert-message">Hatalı İşlem</p></div>',
                    closeBtn: true,
                    floating: {
                        position: "top-right"
                    },
                    focus: true,
                    timer: true ? 2000 : 0
                });
            }

        },
        error: function () {
            $.niftyNoty({
                type: 'danger',
                container: 'floating',
                html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-flash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hata</h4><p class="alert-message">Hatalı İşlem</p></div>',
                closeBtn: true,
                floating: {
                    position: "top-right"
                },
                focus: true,
                timer: true ? 2000 : 0
            });
        }
    });

}
var checkboxes = $(this).closest('#kutucuklar').find('.checkBox');
var checkboxesunread = $(this).closest('#kutucuklar').find('.checkBoxunread');
$("#btnAll").click(function (e) {

    checkboxes.prop('checked', true);
    checkboxesunread.prop('checked', true);

});

$("#btnNone").click(function (e) {
    checkboxes.prop('checked', false);
    checkboxesunread.prop('checked', false);

});

$("#btnAllUnRead").click(function (e) {
    checkboxesunread = $(this).closest('#kutucuklar').find('.checkBoxunread');
    checkboxes = $(this).closest('#kutucuklar').find('.checkBox');

    checkboxes.prop('checked', false);

    checkboxesunread.prop('checked', true);

});

$("#btnAllRead").click(function (e) {
    checkboxesunread = $(this).closest('#kutucuklar').find('.checkBoxunread');
    checkboxes = $(this).closest('#kutucuklar').find('.checkBox');

    checkboxes.prop('checked', true);

    checkboxesunread.prop('checked', false);

});

$("#btnUnRead").click(function (e) {
    var chckedlist = $("input:checkbox.checkBox:checked");
    var chckedlist2 = $("input:checkbox.checkBoxunread:checked");

    if (chckedlist.length > 0 || chckedlist2.length > 0) {
        var updatedItems = new Array();
        $('input:checkbox.checkBox').each(function () {
            if ($(this).prop('checked')) {
                updatedItems.push($(this).val());
            }
        });
        $('input:checkbox.checkBoxunread').each(function () {
            if ($(this).prop('checked')) {
                updatedItems.push($(this).val());
            }
        });
        var postData = JSON.stringify({
            updatedItems
        });
        $.ajax({
            url: "/MBInbox/MailUnRead",
            type: "POST",
            data: { updatedItems: updatedItems },
            dataType: "json",
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            success: function (data) {
                if (data.result==true) {
                    setTimeout(function () { location.reload(); }, 1500);
                    $.niftyNoty({
                        type: 'success',
                        container: 'floating',
                        html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-marker icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Başarılı</h4><p class="alert-message">Okunmadı olarak işaretlendi</p></div>',
                        closeBtn: true,
                        floating: {
                            position: "top-right"
                        },
                        focus: true,
                        timer: true ? 2000 : 0
                    });
                    return false;
                } else {
                    setTimeout(function () { location.reload(); }, 1500);
                    $.niftyNoty({
                        type: 'danger',
                        container: 'floating',
                        html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-flash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hata</h4><p class="alert-message">Hatalı İşlem</p></div>',
                        closeBtn: true,
                        floating: {
                            position: "top-right"
                        },
                        focus: true,
                        timer: true ? 2000 : 0
                    });
                }

            },
            error: function () {
                setTimeout(function () { location.reload(); }, 1500);
                $.niftyNoty({
                    type: 'danger',
                    container: 'floating',
                    html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-flash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hata</h4><p class="alert-message">Hatalı İşlem</p></div>',
                    closeBtn: true,
                    floating: {
                        position: "top-right"
                    },
                    focus: true,
                    timer: true ? 2000 : 0
                });
            }
        });

    }
    else {
        $.niftyNoty({
            type: 'danger',
            container: 'floating',
            html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-cursor-click icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hata</h4><p class="alert-message">Lütfen mesaj seçiniz</p></div>',
            closeBtn: true,
            floating: {
                position: "top-right"
            },
            focus: true,
            timer: true ? 2000 : 0
        });
    }
});

$("#btnRead").click(function (e) {
    var chckedlist = $("input:checkbox.checkBox:checked");
    var chckedlist2 = $("input:checkbox.checkBoxunread:checked");

    if (chckedlist.length > 0 || chckedlist2.length > 0) {
        e.preventDefault();

        var updatedItems = new Array();
        $('input:checkbox.checkBox').each(function () {
            if ($(this).prop('checked')) {
                updatedItems.push($(this).val());
            }
        });
        $('input:checkbox.checkBoxunread').each(function () {
            if ($(this).prop('checked')) {
                updatedItems.push($(this).val());
            }
        });
        var postData = JSON.stringify({
            updatedItems
        });
        $.ajax({
            url: "/MBInbox/MailRead",
            type: "POST",
            data: { updatedItems: updatedItems },
            dataType: "json",
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            success: function (data) {
                if (data.result==true) {
                    $.niftyNoty({
                        type: 'success',
                        container: 'floating',
                        html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-marker icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Başarılı</h4><p class="alert-message">Okundu olarak işaretlendi</p></div>',
                        closeBtn: true,
                        floating: {
                            position: "top-right"
                        },
                        focus: true,
                        timer: true ? 2000 : 0
                    });
                    setTimeout(function () { location.reload(); }, 1500);
                    return false;
                } else {
                    setTimeout(function () { location.reload(); }, 1500);
                    $.niftyNoty({
                        type: 'danger',
                        container: 'floating',
                        html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-flash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hata</h4><p class="alert-message">Hatalı işlem</p></div>',
                        closeBtn: true,
                        floating: {
                            position: "top-right"
                        },
                        focus: true,
                        timer: true ? 2000 : 0
                    });
                }
            },
            error: function () {
                setTimeout(function () { location.reload(); }, 1500);
                $.niftyNoty({
                    type: 'danger',
                    container: 'floating',
                    html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-flash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hata</h4><p class="alert-message">Hatalı işlem</p></div>',
                    closeBtn: true,
                    floating: {
                        position: "top-right"
                    },
                    focus: true,
                    timer: true ? 2000 : 0
                });
            }
        });
    }
    else {
        $.niftyNoty({
            type: 'danger',
            container: 'floating',
            html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-cursor-click icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hata</h4><p class="alert-message">Lütfen mesaj seçiniz</p></div>',
            closeBtn: true,
            floating: {
                position: "top-right"
            },
            focus: true,
            timer: true ? 2000 : 0
        });
    }
});






