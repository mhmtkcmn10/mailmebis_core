﻿
$(function () {
    $('#select_all').change(function () {
        var checkboxes = $(this).closest('#kutucuklar').find('.checkBox');
        if ($(this).prop('checked')) {
            checkboxes.prop('checked', true);
        } else {
            checkboxes.prop('checked', false);
        }
    }
    );
});

$("#btnAll").click(function (e) {
    var checkboxes = $(this).closest('#kutucuklar').find(':checkbox');

    checkboxes.prop('checked', true);

});

$("#btnNone").click(function (e) {
    var checkboxes = $(this).closest('#kutucuklar').find(':checkbox');

    checkboxes.prop('checked', false);

});

$("#btnMailDelete").click(function (e) {
    var chckedlist = $("input:checkbox.checkBox:checked");

    if (chckedlist.length > 0) {
        $("#myModal").modal('show');
    } else {
        $.niftyNoty({
            type: 'danger',
            container: 'floating',
            html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-flash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hata</h4><p class="alert-message">Hatalı işlem</p></div>',
            closeBtn: true,
            floating: {
                position: "top-right"
            },
            focus: true,
            timer: true ? 2000 : 0
        });
    }
});
var DeleteMailSend = function () {

    var deletedItems = new Array();
    $('input:checkbox.checkBox').each(function () {
        if ($(this).prop('checked')) {
            deletedItems.push($(this).val());
        }
    });
    var postData = JSON.stringify({
        deletedItems
    });
    $.ajax({
        url: "/MBSend/DeleteMailSend",
        type: "POST",
        data: { deletedItems: deletedItems },
        dataType: "json",
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        success: function (data) {
            if (data.result==true) {
                $('tr:has(input[name="checkBox"]:checked)').remove();
                $("#myModal").modal("hide");
                $.niftyNoty({
                    type: 'success',
                    container: 'floating',
                    html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-trash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Başarılı</h4><p class="alert-message">İşlem gerçekleşti</p></div>',
                    closeBtn: true,
                    floating: {
                        position: "top-right"
                    },
                    focus: true,
                    timer: true ? 2000 : 0
                });
                setTimeout(function () { location.reload(); }, 2000);
            } else {
                $.niftyNoty({
                    type: 'danger',
                    container: 'floating',
                    html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-flash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hata</h4><p class="alert-message">Hatalı işlem</p></div>',
                    closeBtn: true,
                    floating: {
                        position: "top-right"
                    },
                    focus: true,
                    timer: true ? 2000 : 0
                });
            }
        },
        error: function () {
            $.niftyNoty({
                type: 'danger',
                container: 'floating',
                html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-flash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hata</h4><p class="alert-message">Hatalı işlem</p></div>',
                closeBtn: true,
                floating: {
                    position: "top-right"
                },
                focus: true,
                timer: true ? 2000 : 0
            });

        }
    });
}
