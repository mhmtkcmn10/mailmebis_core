﻿
$(document).on('nifty.ready', function () {

    var btn_Register = $("#btnRegister")

    btn_Register.on("click", function (e) {
        e.preventDefault();

        var name = $("#namex").val();
        var surname = $("#surnamex").val();
        var email = $("#emailx").val();
        var username = $("#usernamex").val();
        var password = $("#passwordx").val();

        if (name == "" || surname == "" || email == "" || username == "" || password == "") {
            $.niftyNoty({
                type: 'danger',
                container: 'floating',
                html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-flash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hatalı Giriş</h4><p class="alert-message">Alanlar boş bırakılamaz</p></div>',
                closeBtn: true,
                floating: {
                    position: "top-right"
                },
                focus: true,
                timer: true ? 2000 : 0
            });
        } else {

            var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;

            if (!$("#emailx").val().match(pattern)) {
                $.niftyNoty({
                    type: 'danger',
                    container: 'floating',
                    html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-flash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hata</h4><p class="alert-message">Email formatı eksiksiz giriniz</p></div>',
                    closeBtn: true,
                    floating: {
                        position: "top-right"
                    },
                    focus: true,
                    timer: true ? 2000 : 0
                });
                return false;
            }

            var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;

            if (!$("#passwordx").val().match(passw)) {
                $.niftyNoty({
                    type: 'danger',
                    container: 'floating',
                    html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-flash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hata</h4><p class="alert-message">Şifreniz 8 ile 15 karakter arasında büyük harf, küçük harf, sayı ve simge içermelidir</p></div>',
                    closeBtn: true,
                    floating: {
                        position: "top-right"
                    },
                    focus: true,
                    timer: true ? 2000 : 0
                });
                return false;
            }

            if ($("#demo-form-checkbox").prop("checked")) {
                var data = {
                    "name": name,
                    "surname": surname,
                    "email": email,
                    "username": username,
                    "password": password
                }
                var valdata = $("#registerFORM").serialize();
                $.ajax({
                    url: "/UserRegister/UserRegister",
                    type: "POST",
                    data: { Name: name, Surname: surname, Email: email, Username: username, Password: password },
                    dataType: "json",
                    contentType: /*"application/json"*/'application/x-www-form-urlencoded; charset=UTF-8',
                    beforeSend: function () {
                        $("#loader").show();
                        $("#anagovde").css({ "opacity": "0.5" });
                    },
                    success: function (response) {
                        if (response.result == true) {
                            $.niftyNoty({
                                type: 'success',
                                container: 'floating',
                                html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-star icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Kayıt İşlemi</h4><p class="alert-message">Kayıt işlemi gerçekleşti. Yönlendiriliyorsunuz...</p></div>',
                                closeBtn: true,
                                floating: {
                                    position: "top-right"
                                },
                                focus: true,
                                timer: true ? 1500 : 0
                            });
                            setTimeout(function () {
                                $.get("/UserLogin/UserLogin", function (data) {
                                    window.location.href = "/UserLogin/UserLogin"
                                });
                            }, 1500);

                        } else {

                            $("#loader").hide();
                            $("#anagovde").css({ "opacity": "1" });

                            $.niftyNoty({
                                type: 'danger',
                                container: 'floating',
                                html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-flash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hatalı Giriş</h4><p class="alert-message">Kayıtlı veri girişi mevcuttur</p></div>',
                                closeBtn: true,
                                floating: {
                                    position: "top-right"
                                },
                                focus: true,
                                timer: true ? 2000 : 0
                            });
                        }
                    },
                    error: function () {
                        $("#loader").hide();
                        $("#anagovde").css({ "opacity": "1" });

                        $.niftyNoty({
                            type: 'danger',
                            container: 'floating',
                            html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-flash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hatalı Giriş</h4><p class="alert-message">Hatalı işlem</p></div>',
                            closeBtn: true,
                            floating: {
                                position: "top-right"
                            },
                            focus: true,
                            timer: true ? 2000 : 0
                        });
                    }
                })
            } else {
                $("#loader").hide();
                $("#anagovde").css({ "opacity": "1" });

                $.niftyNoty({
                    type: 'danger',
                    container: 'floating',
                    html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="psi-flash icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Hatalı İşlem</h4><p class="alert-message">İşaretlenecek alan mevcuttur</p></div>',
                    closeBtn: true,
                    floating: {
                        position: "top-right"
                    },
                    focus: true,
                    timer: true ? 2000 : 0
                });
            }

        }

    })
})
