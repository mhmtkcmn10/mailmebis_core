﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mailNiftymebis_netcore.Entities
{
    public class Child
    {
        public ObjectId Mailid { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }

        public DateTime Date { get; set; }

        public List<MailDetails> MailDetails { get; set; }

        public List<Mail> Mails { get; set; }
    }
}
