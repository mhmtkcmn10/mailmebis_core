﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mailNiftymebis_netcore.Entities
{
    public class UserLoginInfo
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
