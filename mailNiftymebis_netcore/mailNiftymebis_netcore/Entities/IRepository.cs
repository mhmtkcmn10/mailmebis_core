﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mailNiftymebis_netcore.Entities
{
    public interface IRepository<T>
    {
        List<T> findAll();
        T find(string id);

        void create(T entity);

        void update(string id, T entity);

        void delete(string id);

    }
}
