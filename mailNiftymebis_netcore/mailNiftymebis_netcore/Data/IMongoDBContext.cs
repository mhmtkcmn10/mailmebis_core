﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mailNiftymebis_netcore.Data
{
    public interface IMongoDBContext
    {
        
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }

    }
}
